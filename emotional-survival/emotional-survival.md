# Emotional Survival of the Neuro-Atypical

A subjective account.

## Abstract

This essay is an attempt to describe the author's "coping strategies", as they have come to be known, as they are employed to navigate psychiatrists, nurses, accredited school teachers, and other authorities who have complete control; families, friends, colleagues and neighbors; and those with which the author has a parasocial relationship: politicians, philosophers, other authors, and even admired figured who may be more imagination than reality.

## Purpose

It is the purpose, or stated aim, of this essay to describe one human being's strategies with the intent of letting the reader find what they may find useful. The very term "diverse" suggests that there are an uncountable number of variations, so many strategies cannot apply. But, perhaps, some strategies may be useful, directly.

A secondary aim is for those for whom nothing in this writing aids directly. For those readers, the obvious goal is to aid in understanding those who may mystify you.

## Strategy
