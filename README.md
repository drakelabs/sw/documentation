# General Documentation

Here will be found everything from general documentation no included on
specific projects, to treatises and primers on CS subjects, to political
manifestos.

## Organization

Docs are arranged by directory, even if the contents art minimal, to keep meta
in a logical place.

## License

Like most projects in this group, nearly all are Affero-licensed to be
friendly for rehosting.
