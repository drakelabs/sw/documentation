# Quick primer on `nvm`

In this age of of `node`, `electron`, and `web extensions`, `nvm` has become
has become as important as a Python `env`, Ruby equivs. etc.

Fortunately `nvm` works great, besides the the hiccups with `brew` which are
not usually its fault.

## Easiest step one

The easiest step one is to follow the directions on the `README`. The paranoid
(like me) can run the bash script directly instead of from the network, or
just inspect the script.

### Note from me

I liked reading the whole `README`. It's well done, particularly the
troubleshooting, though I haven't used it in years (on both Mac and Linux).

I would not install with `brew`... that is the _only_ time I have had trouble,
and I think it's because the `brew` people had to customize it.

## Start from scratch, or try to move stuff?

When you run the shell script, it will automatically try to migrate everything
to `$HOME/.nvm`. Sometimes this works, and I think that the times that it
doesn't are based on packages being installed under different `node`s and
`npm`s.

In any case, if you've followed advice and gone mostly project-local except
for where you can't (and you can maybe `ll > my-node-packages` or some more
advanced way to get the names and versions of your global packages), and
start **clean**. This is doubly-true if you've ever used root (or `sudo`) to
install anything. This _will screw things up, with or without `nvm`_.

### Start from Scratch

Starting from scratch is great. Uninstall/nuke/delete/your existing `node`
installation (providing there's no data... there shouldn't be under the `lib`
or `opt` directories, but wise to check).

This way, once you've installed `nvm`, all you have to do is the following:
(and this is where `nvm` shines`)

```bash
# shows all versions available for install. May want to use a pager like
# `more` or `less`; you can even install old `io.js` versions!
nvm ls-remote

# btw, `nvm` is a function, like pyenv's `activate`... so `which` won't help.
# If you curious, run `command -V nvm` to see the function. It's in `.nvm/`
# off of home `$HOME`.

# Pick out the version you want. You can't go wrong with the latest LTS. At
# the time of writing, it's `v12.14.1 Erbium`
# The following command installs it (globally).
# Additonally, there are flags to symlink packages, but it's all in the docs.
nvm install --lts
```

Wheee! Installed. Now one more trick.

### Local nvms

When you want a particular version to run with a project, drop a file like
this:
```bash
echo "v12.4.1" >> .nvmrc
```
This should create a file noting which version you prefer. Whenever you want
to activate it, just use
```bash
echo .nvmrc
# v12.4.1

nvm use
```
`nvm` will automatically use that version for that project. Handy.

## Epilogue

I couldn't live without `nvm`, particularly with CI/CD like Circle, where they
actually expect you to use a "convenience Docker image" with a node version
on it and nothing else important. Much more sane to install `nvm`, then put
`nvm use` in our CI script. And, of course, regression testing at home, etc.

Good luck. Let me know if you hit any roadblocks.
